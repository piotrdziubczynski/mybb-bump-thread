<?php

declare(strict_types=1);

$l = [
  'name' => 'Podbij Temat',
  'description' => 'Umożliwia użytkownikom podbicie własnych tematów bez pisania postów.',
  'settinggroups_title' => 'Podbij Temat - Opcje',
  'settinggroups_description' => "Ta sekcja pozwala Ci na zmianę ustawień wtyczki 'Bump Thread'.",
  'setting_time_title' => 'Czas pomiędzy podbiciami',
  'setting_time_description' => "Czas, który użytkownik musi odczekać, zanim będzie mógł (ponownie) podbić temat. Domyślna wartość to '30'.",
  'setting_time_type_title' => 'Typ czasu pomiędzy podbiciami',
  'setting_time_type_description' => "Typ czasu, jaki użytkownik musi odczekać, zanim będzie mógł (ponownie) podbić temat. Wartością domyślną są 'Minuty'.",
  'setting_forums_title' => 'Wybierz fora',
  'setting_forums_description' => "Wybierz fora, na których powinna działać wtyczka. Wartością domyślną jest 'Żadne'.",
  'interval_type_minute' => 'Minuty',
  'interval_type_hour' => 'Godziny',
  'interval_type_day' => 'Dni',
];