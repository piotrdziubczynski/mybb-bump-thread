<?php

declare(strict_types=1);

$l = [
  'bump' => 'Podbij',
  'bump_title' => 'Podbij ten temat',
  'bumped' => 'Temat został podbity.',
  'interval_error' => 'Nie możesz podbić tego wątku w ciągu {1} {2} od ostatniego podbicia.',
  'interval_type_minute' => 'Minut',
  'interval_type_hour' => 'Godzin',
  'interval_type_day' => 'Dni',
];