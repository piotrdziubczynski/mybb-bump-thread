<?php

declare(strict_types=1);

$l = [
  'bump' => 'Bump',
  'bump_title' => 'Bump this thread',
  'bumped' => 'Thread Bumped.',
  'interval_error' => 'You cannot bump this thread within {1} {2} since the last bump.',
  'interval_type_minute' => 'Minute(s)',
  'interval_type_hour' => 'Hour(s)',
  'interval_type_day' => 'Day(s)',
];