<?php

declare(strict_types=1);

$l = [
  'name' => 'Bump Thread',
  'description' => 'It allows users to bump their threads without posting.',
  'settinggroups_title' => 'Bump Thread - Options',
  'settinggroups_description' => "This section allows you to change the settings of the 'Bump Thread' plugin.",
  'setting_time_title' => 'Time Between Bumps',
  'setting_time_description' => "The time users must wait until they are allowed to (re)bump their thread. The default value is '30'.",
  'setting_time_type_title' => 'Time Type Between Bumps',
  'setting_time_type_description' => "The type of time users must wait until they are allowed to (re)bump their thread. The default is 'Minute(s)'.",
  'setting_forums_title' => 'Select Forums',
  'setting_forums_description' => "Select the forums where the plugin should work. The default value is 'None'.",
  'interval_type_minute' => 'Minute(s)',
  'interval_type_hour' => 'Hour(s)',
  'interval_type_day' => 'Day(s)',
];