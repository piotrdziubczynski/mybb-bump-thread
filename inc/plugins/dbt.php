<?php

declare(strict_types=1);

// Disallow direct access to this file for security reasons
if (!defined('IN_MYBB')) {
  die('Direct initialization of this file is not allowed.');
}

require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';

global $plugins;

$plugins->add_hook('datahandler_post_insert_post', 'dbt_newpost');
$plugins->add_hook('datahandler_post_insert_thread', 'dbt_newthread');
$plugins->add_hook('showthread_start', 'dbt');

function dbt_info(): array
{
  global $lang;
  $lang->load('dbt');

  return [
    'name' => $lang->name,
    'description' => implode(' ', [
      $lang->description,
      'This plugin is based on the old \'Bump Thread\' plugin by <i>ZiNgA BuRgA</i>.',
    ]),
    'website' => 'https://gitlab.com/piotrdziubczynski/mybb-bump-thread',
    'author' => '-n3veR',
    'authorsite' => 'https://dziubczynski.pl',
    'version' => '3.0.0',
    'compatibility' => '18*',
  ];
}

function dbt_settinggroups_id(): int
{
  global $db;

  $gid = $db->fetch_field($db->simple_select('settinggroups', 'gid', "name = 'dbt'"), 'gid');

  return (int) $gid;
}

function dbt_interval_types(): array
{
  global $db, $lang;
  $lang->load('dbt');

  return [
    60 => $db->escape_string($lang->interval_type_minute),
    3600 => $db->escape_string($lang->interval_type_hour),
    86400 => $db->escape_string($lang->interval_type_day),
  ];
}

function dbt_interval_types_acp(): string
{
  $types = dbt_interval_types();

  array_walk($types, function (&$value, $key) {
    $value = sprintf('%s=%s', $key, $value);
  });

  return implode('\r\n', $types);
}

function dbt_has_permission(): bool
{
  global $mybb, $thread;

  return $thread['uid'] == $mybb->user['uid'] || (bool) ($mybb->usergroup['cancp'] ?? false);
}

function dbt_activate(): void
{
  find_replace_templatesets('showthread', sprintf('#%s#', preg_quote('{$newreply}')), '{$newreply}{$dbt}');
}

function dbt_deactivate(): void
{
  find_replace_templatesets('showthread', sprintf('#%s#', preg_quote('{$dbt}')), '', 0);
}

function dbt_install(): void
{
  global $lang, $db;
  $lang->load('dbt');

  $db->insert_query('templates', [
    'title' => 'dbt_template',
    'template' => $db->escape_string('<a href="showthread.php?tid=:tid&amp;action=bump" title=":title" class="button bump_thread_button"><span>:text</span></a>'),
    'sid' => -1,
    'version' => 300,
    'dateline' => time(),
  ]);

  $disporder = $db->fetch_field($db->simple_select('settinggroups', 'MAX(disporder) AS disporder'), 'disporder');
  $disporder = (int) $disporder;

  $db->insert_query('settinggroups', [
    'name' => 'dbt',
    'title' => $db->escape_string($lang->settinggroups_title),
    'description' => $db->escape_string($lang->settinggroups_description),
    'disporder' => ++$disporder,
  ]);

  $db->insert_query_multiple('settings', [
    [
      'name' => 'dbt_time',
      'optionscode' => 'numeric',
      'value' => 30,
      'title' => $db->escape_string($lang->setting_time_title),
      'description' => $db->escape_string($lang->setting_time_description),
      'disporder' => 1,
      'gid' => $db->insert_id(),
    ],
    [
      'name' => 'dbt_time_type',
      'optionscode' => sprintf('select \r\n%s', dbt_interval_types_acp()),
      'value' => 60,
      'title' => $db->escape_string($lang->setting_time_type_title),
      'description' => $db->escape_string($lang->setting_time_type_description),
      'disporder' => 2,
      'gid' => $db->insert_id(),
    ],
    [
      'name' => 'dbt_forums',
      'optionscode' => 'forumselect',
      'value' => '',
      'title' => $db->escape_string($lang->setting_forums_title),
      'description' => $db->escape_string($lang->setting_forums_description),
      'disporder' => 3,
      'gid' => $db->insert_id(),
    ],
  ]);

  rebuild_settings();
}

function dbt_is_installed(): bool
{
  return !empty(dbt_settinggroups_id());
}

function dbt_uninstall(): void
{
  global $db;

  $gid = dbt_settinggroups_id();

  if (empty($gid)) {
    return;
  }

  $db->delete_query('templates', "title = 'dbt_template'");
  $db->delete_query('settings', "gid = '{$gid}'");
  $db->delete_query('settinggroups', "gid = '{$gid}'");

  rebuild_settings();
}

function dbt_newpost(\PostDataHandler $handler): void
{
  return;
}

function dbt_newthread(\PostDataHandler $handler): void
{
  return;
}

function dbt(): void
{
  global $lang, $mybb, $thread;
  $lang->load('dbt');

  $forums_allowed = (string) ($mybb->settings['dbt_forums'] ?? '');

  if (empty($forums_allowed)) {
    return;
  }

  $fid = (string) ($thread['fid'] ?? '');

  if ($forums_allowed === '-1') {
    $forums_allowed = $fid;
  }

  if (!in_array($fid, explode(',', $forums_allowed), true)) {
    return;
  }

  $interval = (int) ($mybb->settings['dbt_time'] ?? 0);
  $interval_type = (int) ($mybb->settings['dbt_time_type'] ?? 0);
  $bump = (int) ($thread['lastpost'] ?? 0) + ($interval * $interval_type);

  $action = (string) ($mybb->input['action'] ?? '');

  if (!empty($action) && $action === 'bump') {
    dbt_run($bump, $interval, dbt_interval_types()[$interval_type] ?? '');
    return;
  }

  dbt_show_button($bump);
}

function dbt_run(int $bump, int $interval, string $interval_type): void
{
  global $lang, $thread, $db;
  $lang->load('dbt');

  if ($bump > time()) {
    error($lang->sprintf($lang->interval_error, $interval, $interval_type));
    return;
  }

  if (!dbt_has_permission()) {
    error_no_permission();
    return;
  }

  $tid = (string) ($thread['tid'] ?? '');

  if (empty($tid)) {
    redirect('index.php', '', '', true);
    return;
  }

  $db->update_query('threads', ['lastpost' => time()], "tid={$tid}");
  redirect("showthread.php?tid={$tid}", $lang->bumped);
}

function dbt_show_button(int $bump): void
{
  global $lang, $dbt, $tid, $templates;
  $lang->load('dbt');

  if ($bump > time() || !dbt_has_permission()) {
    return;
  }

  $template = stripslashes($templates->get('dbt_template'));
  $dbt = str_replace([':tid', ':title', ':text'], [$tid, $lang->bump_title, $lang->bump], $template);
}