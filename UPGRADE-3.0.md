UPGRADE from 2.x to 3.0
=========================

Admin Control Panel
----------------------
* Go to the Plugin page and deactivate plugin.

Filesystem
-------------
* Remove `inc\languages\english\admin\d_bumpthread.lang.php`.
* Remove `inc\languages\english\d_bumpthread.lang.php`.
* Remove `inc\languages\polish\admin\d_bumpthread.lang.php`.
* Remove `inc\languages\polish\d_bumpthread.lang.php`.
* Remove `inc\plugins\d_bumpthread.php`.

(re)Instalation
--------------
1. Download the new version.
2. Unpack ZIP file.
3. Move files into directories.
4. Install and activate the plugin.
5. Go to the Settings page and set up options as you wish.
6. _(optional)_ to change the button appearance you can overwrite the style by using a CSS class: `bump_thread_button` or _more globally_ `button`.

**!important information**
Plugin in the newest version is disabled on all Forum as default.