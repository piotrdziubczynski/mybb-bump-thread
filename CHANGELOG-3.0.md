CHANGELOG for 3.0.x
===================

_Tested on clean MyBB v1.8.21_

Added:
------
* Compatibility for PHP v7.x.
* URL address for plugin.
* Separate functions for **(de)activate** and **(un)install**.
* Other useful functions.

Removed:
--------
* Compatibility for PHP v5.x.
* `eval()` function for generate button view.
* Unnecessary code for hooks for **new post** and **new thread**.
* Duplicated code.

Changed:
--------
* Plugin file name.
* Translation files names.
* Translation keys.
* Template variables.
* Permissions for bump thread. Only Author and Admin can do this action now.
* and more other, small changes...