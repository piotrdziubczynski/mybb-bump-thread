# Bump Thread
It allows users to bump their threads without posting. This plugin is based on the old 'Bump Thread' plugin by _ZiNgA BuRgA_.

Installation
--------------
1. Download the new version.
2. Unpack ZIP file.
3. Move files into directories.
4. Install and activate the plugin.
5. Go to the Settings page and set up options as you wish.
6. _(optional)_ to change the button appearance you can overwrite the style by using a CSS class: `bump_thread_button` or _more globally_ `button`.

**!important information**
The plugin adds own variable `$dbt` in the **showthread** template:
```
<div style="padding-top: 4px;" class="float_right">
    {$newreply}{$dbt}
</div>
```
If you are using a custom theme and you still do not see the button after changing options, please check the variable in the template above.

Screenshots
--------------

**ACP Options**
![ACP Options](https://salvation.ct8.pl/plugins/mybb/admin-options.png)

**Thread View**
![Bump Button](https://salvation.ct8.pl/plugins/mybb/bump-button.png)

**Message**
![Message](https://salvation.ct8.pl/plugins/mybb/message.png)

Filesystem Tree
------------------

```
inc/
    languages/
        english/
            admin/
                dbt.lang.php
            dbt.lang.php
        polish/
            admin/
                dbt.lang.php
            dbt.lang.php
    plugins/
        dbt.php
```